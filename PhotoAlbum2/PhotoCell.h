//
//  PhotoCell.h
//  PhotoAlbum2
//
//  Created by amttgroup on 15-5-18.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
