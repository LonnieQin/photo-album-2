//
//  PhotosViewController.m
//  PhotoAlbum
//
//  Created by amttgroup on 15-5-18.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "PhotosViewController.h"
#import "PhotoCell.h"
#import "LQCoverFlowLayout.h"
#import "DetailViewController.h"
enum PhotoOrientation{
    PhotoOrientationLandScape,
    PhotoOrientationPortrait
};
@interface PhotosViewController()
@property (strong,nonatomic) NSArray * photosList;
@property (strong,nonatomic) NSCache * photosCache;
@end
@implementation PhotosViewController
- (NSString*) photosDirectory
{
    return [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Photos"];
}


- (void) viewDidLoad
{
    [super viewDidLoad];
    NSArray * photosArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[self photosDirectory] error:nil];
    self.photosCache = [NSCache new];
 
    self.photosList = nil;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,0), ^{
        [photosArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSString * path = [[self photosDirectory] stringByAppendingPathComponent:obj];
            CGSize size = [UIImage imageWithContentsOfFile:path].size;
 
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.photosList = photosArray;
            [self.collectionView reloadData];
        });
    });
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    self.photosCache = [NSCache new];
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.photosList count];
}

- (UICollectionViewCell*) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifierLandscape = @"Photo";
    PhotoCell * cell = (PhotoCell*)[collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifierLandscape forIndexPath:indexPath];
    NSString * photoName = [[self photosList] objectAtIndex:indexPath.row];
    NSString * photoFilePath = [[self photosDirectory] stringByAppendingPathComponent:photoName];
    __block UIImage * thumbImage = [self.photosCache objectForKey:photoName];
    cell.imageView.image = thumbImage;
    
    LQCoverFlowLayout * layout = (LQCoverFlowLayout*)collectionView.collectionViewLayout;
    if (!thumbImage) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            UIImage * image = [UIImage imageWithContentsOfFile:photoFilePath];
            float scale = [UIScreen mainScreen].scale;
            UIGraphicsBeginImageContextWithOptions(layout.itemSize, YES, scale);
            [image drawInRect:CGRectMake(0, 0, layout.itemSize.width, layout.itemSize.height)];
            thumbImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.photosCache setObject:thumbImage forKey:photoName];
                cell.imageView.image = thumbImage;
            });
        });
    }
    return cell;
}

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (BOOL) collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL) collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"detail" sender:indexPath];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath * selectedIndexPath = sender;
    NSString * photoName = [self.photosList objectAtIndex:selectedIndexPath.row];
    DetailViewController * controller = segue.destinationViewController;
    controller.photoPath = [[self photosDirectory] stringByAppendingPathComponent:photoName];
}


@end
