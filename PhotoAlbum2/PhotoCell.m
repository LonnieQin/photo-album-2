//
//  PhotoCell.m
//  PhotoAlbum2
//
//  Created by amttgroup on 15-5-18.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "PhotoCell.h"

@implementation PhotoCell
-(void) awakeFromNib {
    
    self.selectedBackgroundView = [[UIView alloc] initWithFrame:self.frame];
    self.selectedBackgroundView.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.5];
    
    self.imageView.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.imageView.layer.borderWidth = 5.0f;
    
    [super awakeFromNib];
}
@end
