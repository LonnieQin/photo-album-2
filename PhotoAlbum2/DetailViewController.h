//
//  DetailViewController.h
//  PhotoAlbum
//
//  Created by amttgroup on 15-5-18.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController
@property (nonatomic,strong) NSString * photoPath;
@end
