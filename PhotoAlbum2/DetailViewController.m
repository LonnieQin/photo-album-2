
//
//  DetailViewController.m
//  PhotoAlbum
//
//  Created by amttgroup on 15-5-18.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "DetailViewController.h"
@interface DetailViewController()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
@implementation DetailViewController
- (void) viewDidLoad
{
    [super viewDidLoad];
    self.nameLabel.text= [[self.photoPath pathComponents][[self.photoPath pathComponents].count-1] stringByDeletingPathExtension];
    self.imageView.image = [[UIImage alloc] initWithContentsOfFile:self.photoPath];
    NSLog(@"%s %@",__func__,self.photoPath);
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
